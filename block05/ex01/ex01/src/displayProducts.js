import React from 'react';

class ShoppingCart extends React.Component {


displaySelectedProducts = (props) => {
 return (
    <div>
    {console.log(this.props)}
    {this.props.selectedProducts.items.map ((item, key)=>
    <div  style={selection}>
        <p style={itemPart}>{this.props.selectedProducts.items[key].name}</p>
        <p style={itemPart}>{this.props.selectedProducts.items[key].price}</p>
    </div>
        )}
        
        
    </div>
 )
}

displayAllProducts = (props) => {
    return (
        <div>
            
             <h1>All items</h1>
            {console.log(this.props)}
            {this.props.allProducts.items.map ((item, key)=>
            <div  style={selection}>
                <p style={itemPart}>{this.props.allProducts.items[key].name}</p>
                <p style={itemPart}>{this.props.allProducts.items[key].categorie}</p>
                <p style={itemPart}>{this.props.allProducts.items[key].price}</p>
            </div>
                )}
        </div>
    )
}

    render () {
        return (
            <div style={Display}>
            {this.props.setCategorie === 'start' && this.props.count === 0 ? 
                <div> 
                    <p style={info}>No items selected</p>
                    {this.displayAllProducts()}
                </div>
                        : this.props.setCategorie === 'all' && this.props.count === 0 ? 
                        <div>
                            <p style={info}>All items selected</p>
                            {this.displayAllProducts()}
                        </div>
                        
                                : this.props.count === 0 ? 
                                    <div>
                                        <p style={info}>Categorie not found</p>
                                        {this.displayAllProducts()}
                                    </div>
                   
                                        : <div>
                                            <p style={info}> Selected Products </p>
                                            { this.displaySelectedProducts()}
                                        </div>
                                       
                 }
           
           
            </div>
            
        )
    }


}

const Display = {
    margin: "5% 0"
  }

const selection = {
    display: 'inline-flex',
    borderStyle: 'solid',
    borderColor: 'blue',
    borderWidth: '1px', 
    margin: '3% 3% 3% 0'
}  

const itemPart = {
    padding: '0 5%',
    width: '10vw'
}

const info = {
    padding: '1% 5%',
    width: '10vw',
    color: 'red',
    borderStyle: 'solid',
    borderColor: 'orange',
    borderWidth: '1px'
}

export default ShoppingCart; 

// const ShoppingCart = (props) => {
    //     if (props.items.length === 0 ){ 
    //         return (
    //             <div>
    //                 <h3>Nothing in your Cart</h3>
    //             </div>
    //         )
    //     } else {
    //         return (
    //             <div className="shopppingCart">
    //                 name = {props.name}
    //                 price = {props.price}
    //                 color = {props.color}
    //                 categorie = {props.categorie}
    //             </div>
    //         )
    //     } 
    // }