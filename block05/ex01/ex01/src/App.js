import React from 'react';
import DisplayProducts from './displayProducts'; 



class App extends React.Component {

state = {
  items: [],
  count: 0,
  setCategorie: 'start'
}


products = { 
  items: [
          { 
          name: 'boots',
          categorie : 'shoes', 
          price: 99.95,
          quantity: 1,
          color: 'black',
          },

          { 
          name: 'pumps',
          categorie : 'shoes', 
          price: 229.95,
          quantity: 1,
          color: 'red',
          },

          {
          name: 'jeans',
          categorie: 'pants',
          price: 59.95,
          quantity: 1,
          color: 'blue',
          },

          {
          name: 'tv',
          categorie: 'electronica',
          price: 455.95,
          quantity: 1,
          color: 'black',
          }
      ]
  }

lookForCategorie = (e)  =>  {
    
    let newItems = []; 
    e.preventDefault()
    let setCategorie = e.target.firstElementChild.value

          let count = 0 
          this.products.items.map ( (item, key) =>
            this.products.items[key].categorie === setCategorie ? 
               
            (
              count += 1 && 
              newItems.push ({
                    name : this.products.items[key].name,
                    price :  this.products.items[key].price,
                    quantity : this.products.items[key].quantity
                  })
                
            )
            :null

          )  

            this.setState ({
                items : newItems,
                count: count, 
                setCategorie: setCategorie
            })
}



  render () {
    return (
      <div style={app}>
      <h1 style={h1}>Search for your categorie</h1>
          <form onSubmit={this.lookForCategorie}>
              Categorie: <input></input>
         <button>Search</button>
       </form>
      <DisplayProducts  allProducts={this.products} selectedProducts={this.state} count ={this.state.count} setCategorie = {this.state.setCategorie}/> 
      </div>
      
  
      
    )
  }
}


const app = {
  margin : "5% 5%",
  fontFamily: 'Trebuchet MS', 
}

const h1 = {
  margin: "2% 0"
}





export default App







// import React from 'react';
// import Data from './data'; 
// // import DisplayProducts from './displayProducts'
// // import './App.css';

// class App extends React.Component {
    
// state = {
//   categorie: ''
// }


//   setCategorie = (event) => {
//     event.preventDefault();
//     var categorie = event.target.firstElementChild.value
//     this.setState ({
//       categorie : categorie 
//     })
  
//   }

// render () {

//       return (
//         <div>
//           <h1>Look for your articles</h1>
//           <form onSubmit = {this.setCategorie.bind(this)} >
//             <input></input>
//             <button onClick={()=>this.handleClick()}>Search</button>
//           </form>
//           <Data categorie = {this.state.categorie}/> 
//           {console.log(this.state.categorie)}
//       </div>
//       )
//     }


// }

// export default App
      

// const lookUpCategorie = (value) => {
//   state.categorie === value ? console.log (state) : console.log("STOP"); 
//   setState({...state[0], [state.categorie]:'sweater'});
//   console.log(state); 
// }  

//   return (
//     <div>
//       <p>You clicked {count} times</p>
//       <button onClick={() => setCount(count + 1)}>
//         Click me
//       </button>
//       <input onChange={() => lookUpCategorie() } />
//     </div>
//   );
// }

// export default Example; 




// const Form = () => {
//   const [values, setValues] = useState({ email: "", password: "" });

//   const onChange = (name, value) => {
//     setValues({ ...values, [name]: value });
//   };

//   return (
//     <form onSubmit={e => e.preventDefault() || alert(JSON.stringify(values))}>
//       <Input
//         name="email"
//         placeholder="e-mail"
//         type="email"
//         value={values.user}
//         onChange={onChange}
//       />
//       <Input
//         name="password"
//         placeholder="password"
//         type="password"
//         value={values.password}
//         onChange={onChange}
//       />
//       <input type="submit" />
//     </form>
//   );
// };





// function Cart () { 

//   function selectCatergorie (event) {
//     const choosenCategorie = event.target.value;
//     console.log(choosenCategorie);
//     setCategorie(choosenCategorie); 
//     setState({categorie:choosenCategorie}); 
//     console.log(categorie)
  
//     return (
//       <div>{console.log("all items with this categorie")}</div>
//     )
//   }

//   const [categorie, setCategorie] = useState ('')
//   // const [name, setName] = useState ('testName'); 
//   // const [price, setPrice] = useState ('testPrice');
//   // const [color, setColor] = useState ('testColor');
//   const [state, setState] = useState ([
//     {
//       id: 1,
//       categorie: 'shoes',
//       name: 'boots',
//       price: 99.95,
//       quantity: 1,
//       color: 'black'
//       },
//       {
//       id: 2, 
//       categorie: 'pants',
//       name: 'jeans',
//       price: 59.95,
//       quantity: 1,
//       color: 'blue'
//       },
//       {
//       id: 3,
//       categorie: 'electronica',
//       name: 'tv',
//       price: 455.95,
//       quantity: 1,
//       color: 'black'
//       }
//   ]);


// return ( 
//   <div className="cart">
//     <h1>Choose your categorie</h1>
//     <input onChange={() => selectCatergorie}></input>
//     <div className ="items">{state}</div>
//   </div>
// ) 


// }; 


// export default Cart





// ## Exercise 1

// Create a form that contains an input and a button.

// We are going to use the input to enter a category name and display all products belonging to this category.

// **Requirements**

// - The list of products will be an array of objects, you can use one from the previous exercises.
// - Initially you should display the list of all products.
// - If there are no products in this category a NotFound component will show a message.
// - By typing 'all' in the input we should be able to see the entire list of products again from all the categories.
