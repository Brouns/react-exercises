import React from 'react'

const NewTodoForm = (props) => {
    return (
            <form onSubmit ={props.formSubmitted}>
                <label htmlFor="newTodo"></label>
                <h2>Your new todo:</h2>
                <input className="newTodoForm"onChange={props.newTodoChanged} id="newTodo" name="newTodo" value={props.newTodo}/>
                <button className="plusButton" type="submit">+</button>
            </form>
    )
}; 

export default NewTodoForm; 