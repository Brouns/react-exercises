import React from 'react';
import './App.css';
import NewTodoForm from './NewTodoForm'; 
import TodoList from './TodoList'; 

class App extends React.Component {
  constructor () {
    super ();
    this.state = {
      newTodo : '',
      todos: [{
        title: 'Learn react',
        done:false,
        click: 0,
        hoover: false
        }, {
         title:'Do laundry',
         done: false,  
         click: 0,
         hoover: false
        }
      ] 
    }; 
  }
 
newTodoChanged (event) {
  this.setState ({
    newTodo: event.target.value
  });
}

formSubmitted (event) {
  event.preventDefault(); 
  
  this.setState ({
    newTodo : '' ,
    todos: [...this.state.todos, {
      title: this.state.newTodo,
      done: false,
      click: 0,
      hoover: false,
    }]
  });  
}

toggleTodoDone (event, index) {
  const todos = [...this.state.todos]; //copy array
  todos[index] = {...todos[index]}; // copy todo
  todos[index].click = todos[index].click + 1; 
  console.log(todos[index].click);
  console.log(todos[index].done); 

  this.setState ({
    todos 
  }); 
} 


removeTodo (index) {
  const todos = [...this.state.todos] ; //copy array
  todos.splice(index,1); //delete item
  this.setState ({
    todos 
  }); 
}
 
moveOver (event, index) {
  event.preventDefault(); 
  const todos = [...this.state.todos]; //copy array
  todos[index] = {...todos[index]}; // copy todo
  todos[index].hoover = true; 
  console.log(todos[index].hoover)

  this.setState ({
     todos 
     }); 

  }

moveOut (event,index) {
  event.preventDefault(); 
  const todos = [...this.state.todos]; //copy array
  todos[index] = {...todos[index]}; // copy todo
  todos[index].hoover = false; 
  console.log(todos[index].hoover)

  this.setState ({
     todos 
     }); 
}

render () {
  return (
    <div className="App">
      <h1>My awesome todo list</h1>
      <NewTodoForm
        newTodo = {this.state.newTodo}
        formSubmitted = {this.formSubmitted.bind(this)}
        newTodoChanged = {this.newTodoChanged.bind(this)}
      />
      <TodoList 
        todos = {this.state.todos}
        toggleTodoDone = {this.toggleTodoDone.bind(this)}
        removeTodo = {this.removeTodo.bind(this)}
        moveOver = {this.moveOver.bind(this)}
        moveOut = {this.moveOut.bind(this)}
        />
      
    </div>
  ); 

  }
}

export default App;

// import React from 'react';
// import './App.css';
// import NewTodoForm from './NewTodoForm'; 
// import TodoList from './TodoList'; 

// class App extends React.Component {
//   constructor () {
//     super ();
//     this.state = {
//       newTodo : '',
//       todos: [{
//         title: 'Learn react',
//         done:false
//         }, {
//          title:'Do laundry',
//          done: false 
//         }
//       ] 
//     }; 
//   }
 
// newTodoChanged (event) {
//   this.setState ({
//     newTodo: event.target.value
//   });
// }

// formSubmitted (event) {
//   event.preventDefault(); 
  
//   this.setState ({
//     newTodo : '' ,
//     todos: [...this.state.todos, {
//       title: this.state.newTodo,
//       done: false
//     }]
//   });  
// }

// toggleTodoDone (event, index) {
//   console.log(event.target.checked);
//   const todos = [...this.state.todos]; //copy array
//   todos[index] = {...todos[index]}; // copy todo
//   todos[index].done = event.target.checked; //update done on copy todo
//   this.setState ({
//     todos 
//   }); 
// } 

// removeTodo (index) {
//   const todos = [...this.state.todos] ; //copy array
//   todos.splice(index,1); //delete item
//   this.setState ({
//     todos 
//   }); 
// }
 
// render () {
//   return (
//     <div className="App">
//       <h1>My awesome todo list</h1>
//       <NewTodoForm
//         newTodo = {this.state.newTodo}
//         formSubmitted = {this.formSubmitted.bind(this)}
//         newTodoChanged = {this.newTodoChanged.bind(this)}
//       />
//       <TodoList 
//         todos = {this.state.todos}
//         toggleTodoDone = {this.toggleTodoDone.bind(this)}
//         removeTodo = {this.removeTodo.bind(this)}
//         />
      
//     </div>
//   ); 

//   }
// }

// export default App;