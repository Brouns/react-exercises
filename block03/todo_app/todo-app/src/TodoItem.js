import React from 'react';
import Logo from './images.png'

const TodoItem = (props) => {
    const { todo, index } = props;

    return (
        <li className="item"
        onMouseEnter= { (event) => props.moveOver(event, index)}
        onMouseLeave= { (event) => props.moveOut(event, index)}
            >
           <div className ="todo" 
                onClick= { (event) => props.toggleTodoDone(event, index)} 
                        style={{ textDecoration: todo.click % 2 === 0 ? ('inherit') : ('line-through')} }
         
                > 

                {todo.title} 
            </div>
       
            <div className="trash-button">
                { todo.hoover ? (<button className="button" 
                                        onClick = {() => props.removeTodo(index)}
                                    >
                                        <img className="trashcan" src= {`${Logo}`}/>
                                    </button>)
                                : null }
            </div>
    </li>   
                            

                
                
            
            
      
    ); 
};

export default TodoItem

///I can now be on the item and the trash button at the same time, and that causes an issue..




// export default class MyInput extends Component {
//     // Triggered when the value of the text input changes...
//     onChange() {
//       console.log('changed');
//     }
//     // Triggered when the text input loses focus...
//     onBlur() {
//       console.log('blurred');
//     }
//     // JSX elements can have as many event handler properties as necessary.
//       render() {
//         return <input onChange={this.onChange} onBlur={this.onBlur} />;
//       }
//     }




//VERSION 1

// import React from 'react';

// const TodoItem = (props) => {
//     const { todo, index } = props;
//     return (
//         <li className="item">
//             <input onChange= {(event) => props.toggleTodoDone(event, index)} type="checkbox" />
//             <span style={{ textDecoration: todo.done ? ('line-through') : ('inherit') }}>{todo.title}</span> 
//             {/* <span className={todo.done ? 'done' : ''}>{todo.title}</span> */}
//             <button className="button" onClick = {() => props.removeTodo(index)}>Remove</button>
//         </li>
//     ); 
// };

// export default TodoItem



//VERSION 2

// const TodoItem = (props) => {
//     const { todo, index } = props;

//     return (
//         <li className="item">
//            <div className ="todo" 

//                 onClick= { (event) => props.toggleTodoDone(event, index)} 
//                     style={{ textDecoration: todo.click % 2 === 0 ? ('inherit') : ('line-through') }}

//                 onMouseOver= { (event) => props.moveOver(event, index)}

//                 onMouseOut= { (event) => props.moveOut(event, index)}

                   
//                 >
//                     {todo.hoover ? (<button className="button" 
//                                         // onClick = {() => props.removeTodo(index)}
//                                         >
//                                             <img className="trashcan" src= {`${Logo}`}/>
//                                     </button>)

//                                 : null }
            

//             {todo.title} 
                 
//             </div> 

        
           
//         </li>
//     ); 
// };

// export default TodoItem