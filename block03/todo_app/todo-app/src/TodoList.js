import React from 'react';
import TodoItem from './TodoItem';


const TodoList = (props) => {
    return (
        <ul className="list">
            {props.todos.map((todo, index) => {
                return (
                <TodoItem
                    key={index}
                    index={index}
                    todo={todo}
                    toggleTodoDone={props.toggleTodoDone}
                    moveOver={props.moveOver}
                    moveOut={props.moveOut}
                    removeTodo={props.removeTodo}
                   
                />)
                })}
            </ul>

    );
};

export default TodoList