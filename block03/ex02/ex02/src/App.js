
// onChange Event
import React, { useState } from 'react';
import './App.css';

const App = () => {
  const [data, setData] = useState('');
  const handleChange = (element) => setData(element.target.value)
  
    return (
      <div>
      <input onChange = {handleChange}/>
      <h1>{data}</h1>
      {data === '' ? <h1>No data</h1> : null}
      </div>

      );
  }


export default App;

//worked


// // onChange Event
// import React, {useState , setState} from 'react';
// import './App.css';

// const App = (props) => {
//   const [user,setUser] = useState(undefined)

//     function handleChange (event) {
//       console.log(user);
//       console.log(setUser);
//       setState({[user]:setUser});
//     }
  
//     return (
//       <div onChange = {handleChange()}>
//       <input 
//       name ='data'
//       value ={props} 
//       />
//       <h1>{props}</h1>
//       {props === '' ? <h1>No data</h1> : null}
//       </div>
//         )
    
// }

// export default App



// import React from 'react';

// export default class App extends React.Component {

//   state = { text:'', shown:'' }
//   // in state we define initial empty values

//   handleChange = e =>{
//     this.setState({[e.target.name]:e.target.value})
//     // here we update state for every character entered by user
//   }

//   handleShow = ()=> {
//     this.setState({shown:this.state.text},()=>{
//       // here we clone value from 'text' to 'shown' in the state object
//       this.setState({text:''})
//       // and clear the 'text' value in the state which also clears the input on the page
//     })
//   }
//   render() {
//     return (
//       <div>
//         <input 
//         onChange = {this.handleChange}
//         name = "text" 
//         value = {this.state.text}/>
//       {/* the value or visible text in this input is coming from the state itself */}
//         <h1>{this.state.shown}</h1>
//       {/* this h1 as also coming form the state */}
//         <button onClick={this.handleShow}>show text</button>
//       </div>
//       );
//   }
// }

