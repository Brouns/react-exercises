// onChange Event
import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    data: ''
  }

  handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    //let { name, value } = event.target;
    this.setState({[name]:value});
   }
  
  render() {
    return (
      <div onChange = {this.handleChange}>
      <input 
      name= 'data'
      value ={this.state.data} 
      />
      <h1>{this.state.data}</h1>
      {this.state.data === '' ? <h1>No data</h1> : null}
      </div>
      );
  }
}

export default App;


