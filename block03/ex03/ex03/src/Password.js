import React from 'react';

const Password = (props) =>  {
    return (
        <div>
        <h2> Password: </h2>
        <input className="input" value = {props.passwordValue} onChange={ (event) => props.newPassword(event)}></input>
        </div>
    )
} 

export default Password

