import React from 'react';
// import axios from 'axios';
import './App.css';
import ChangeClass from './ChangeClass'; 
import Alert from './alert'; 

class App extends React.Component {
  state = {
    hidden: true, 
    newAccount: [
      {
        email: "",
        password: "",
        membership: false
      }
    ],
    users: [
      {
      email:"test",
      password:"1234"
      }, {
      email:"bever@gmail.com",
      password:"5678"
      }
    ],
    timer : undefined 
}

componentDidMount() {
    this.interval = setInterval(() => {
      this.setState ({
        timer : this.state.timer + 1 })}, 1000) 
      }; 
    

componentWillUnmount() {
    clearInterval(this.state.interval);
  }

newEmail (event) {
  let email = event.target.value;
  const newAccount = [...this.state.newAccount]
  newAccount[0].email = email; 
  
  this.setState ({
    newAccount
  })
}

newPassword(event) {
  let password = event.target.value;
  const newAccount = [...this.state.newAccount]
  newAccount[0].password = password; 

  this.setState ({
    newAccount
  })
}


submit () {
  let newAccount = [...this.state.newAccount]
  let membership = false; 
  let hidden = false;
  let timer = 0; 

  this.state.users.map (index=> {
    return (
        (index["email"] === this.state.newAccount[0]["email"] && index["password"] === this.state.newAccount[0]["password"]) ? membership = true : null)})    
        newAccount[0].membership = membership; 
  
 
    this.setState ({
      newAccount, 
      hidden,
      timer 
    })
    console.log(hidden) 
}



render () {
    return (
    <div>
      <h1 className="title"> The secret society </h1>

      <div className="input">
          email: <input onChange= {this.newEmail.bind(this)} className="email"/>
          password:  <input onChange= {this.newPassword.bind(this)} className="password"/> 
      </div>

      <button onClick= {this.submit.bind(this)} className="submit">Log In</button>

      { this.state.hidden === true ? null : 
            <div className="Messages">
              { this.state.newAccount[0].membership === true ? 
              <div className="positiveMessages">
                 {this.state.timer ===1 ? <h1>Welcome you are a member</h1> :
                  this.state.timer === 2 ? <h1>Welcome you are </h1> :
                  this.state.timer === 3 ? <h1>Welcome you </h1> :
                  this.state.timer === 4 ? <h1>Welcome </h1> :
                 this.state.timer > 4 ? <ChangeClass /> : null }
                </div> 
            : 
            <div className="negativeMessages"> <h1>Your not a member</h1> </div>
            }
          </div>

    
       }

   </div> 

    )

}
}

export default App;


//Can't make the message to go away.... 


// ## Exercise 3

// Create a form that gets email and password and a component with an alert initially not visible. 
// When submit it should check in an array of users if the email exists and if so, if the corresponding password is correct. 

// If email and password are correct you should display a positive message with a green background 
//otherwise negative message and red background but in both cases a series of messages:

//     This message will desappear in 3
//     This message will desappear in 2
//     This message will desappear in 1
//     This message will desappear in 0

// and make the alert component invisible again i.e. unmount it.

// > Use `setInterval` to display every message for 1 second

