import React from 'react';
import './App.css';
import DisplayItem from './DisplayItem'; 
import TotalCosts from './TotalCosts'; 
// import Amount from './Amount'

class Cart extends React.Component { 
  state = {
      products: [
          {
          id: 1,
          name: 'boots',
          price: 99.95,
          quantity: 1
          },
          {
          id: 2, 
          name: 'jeans',
          price: 59.95,
          quantity: 2
          },
          {
          id: 3,
          name: 'tv',
          price: 55.95,
          quantity: 3
          }
      ]
    } 
  
updateAmount (quantitySet, key) {
  console.log(this.state.products[key])
  let updateAllItems= [...this.state.products]
  let updateItem =[...this.state.products[key]]
  //So here I need to know the key of the specific product

  updateItem.quantity = quantitySet
  updateAllItems[key] = updateItem
  this.setState({
    products :  updateAllItems
  })
}

render () {
  return (

    <div className="cart">
      <h1>Your shopping cart</h1>
      <ul>
          {this.state.products.map((item, key) => {
            return (
              <DisplayItem 
                    item = {this.state.products[key]} updateAmount = {this.updateAmount.bind(this)} key={key}/>
            )
       })}
      </ul>
       <TotalCosts products = {this.state.products} />
  </div>
  )
}

}

export default Cart


// function Product (props) {
//   const [quantity, setQuantity] = useState (1);

//   const ChangeAmount = e => {
//     e.preventDefault(); 
//     if (!value) return;
//       setQuantity({quantity})
//       props.products.quantity = parseInt(value); 
//     setValue(''); 
    
//   }
//     return (
//       <div className="product">
//           <p>Product: {props.products.name}</p>
//           <p>Price: ${props.products.price}</p>
//           <p>Amount: {props.products.quantity}</p>

//         <form onSubmit={ChangeAmount}>
//               Change amount to:<input className="input" defaultValue="1" onChange={e => setValue(e.target.value)}  value={value}></input>
//       </form>
//       {/* <Total products={products} quantity={props.products.quantity}/>  */}
//       </div>
//     )
//   }


//   function Total (props) {
   
//     let total = 0;
//     console.log(total);  
//       props.products.map((product) => (
//         console.log(product.quantity)
//         // total += (product.price * product.quantity)
//       ))

//       // It doesn't change the quantity in the state and therefore it doesn't update the Total amount after a quantity is changed..

      
//     console.log(props.products);
//     console.log(total); 
//     let roundedTotal = total.toFixed(2); 

//         return (
//           <div className ="total">
//           <h3>Total: {roundedTotal} </h3>
//           {total > 500 ? <div className="free-shipping"> Free Shipping! </div> 
//               : <div className="no-free-shipping"> No free shipping, yet... (from $500)</div> }
//           </div>
//         )
//   }


// function Cart () {
//   const [products, setProducts] = useState ([
//     {
//       id: 1,
//       name: 'boots',
//       price: 99.95,
//       quantity: 1
//       },
//       {
//       id: 2, 
//       name: 'jeans',
//       price: 59.95,
//       quantity: 1
//       },
//       {
//       id: 3,
//       name: 'tv',
//       price: 455.95,
//       quantity: 1
//       }
//   ]);



//   return (
//     <div className="cart">
//     <h1>Your shopping cart</h1>
//       <div className ="product-list">
//         {products.map((products, index) => (
//           <Product key={index} index={index} products={products} />
//         ))}
//         </div>
//       <div className="total">{
//         <Total products={products}/>
//       }</div>  
//     </div>
//   )
  
// }

// export default Cart;


//




// ## Exercise 4

// Create a cart component that will display a list of products in cart. 
// For the products you can use the same array from the products page but with a new key value pair for the quantity.

// Requirements:

// - you have to use React Hooks for this exercise
// - you should display the total cost of the products
// - you should be able to change the product's quantity
// - you should display a free shipping green message once the total gets to 500€ 
// or more, otherwise not eligible for a free shipping red message.










// import React, {useState} from 'react';
// import './App.css';



// function Cart () {
//   const [products, setProducts] = useState ([
//     {
//       id: 1,
//       name: 'boots',
//       price: 99.95,
//       quantity: 0
//       },
//       {
//       id: 2, 
//       name: 'jeans',
//       price: 59.95,
//       quantity: 1
//       },
//       {
//       id: 3,
//       name: 'tv',
//       price: 455.95,
//       quantity: 1
//       }
//   ]);

//   return (
//     <div className="cart">
//     <h1>Your shopping cart</h1>
//       <div className ="product-list">
//         {products.map((products, index) => (
//           <Product key={index} index={index} products={products} />
//         ))}
//         </div>
//       <div className="total">{
//         <Total products={products}/>
//       }</div>  
//     </div>
//   )
  
// }

// function Product (props) {
//   const [value, setValue] = useState (); 

//   const ChangeAmount = e => {
//     e.preventDefault(); 
//     if (!value) return;
//       props.products.quantity = value; 
//     setValue(''); 
//   }

//     return (
//       <div className="product">
//           <p>Product: {props.products.name}</p>
//           <p>Price: ${props.products.price}</p>
//           <p>Amount: {props.products.quantity}</p>

//         <form onSubmit={ChangeAmount}>
//               Change amount to:<input className="input" defaultValue="1" onChange={e => setValue(e.target.value)}  value={value}></input>
//       </form>
//       </div>
//     )
// }

// function Total (props) {
//   let total = 0; 
//   console.log(total)
//   props.products.map((product) => (
//     total += (product.price * product.quantity),
//     console.log(product.quantity)
//   ))

//   let roundedTotal = total.toFixed(2)
  
//       return (
//         <div className ="total">
//         <h3>Total: {roundedTotal} </h3>
//         {total > 500 ? <div className="free-shipping"> Free Shipping! </div> 
//             : <div className="no-free-shipping"> No free shipping, yet... (from $500)</div> }
//         </div>
//       )
// }