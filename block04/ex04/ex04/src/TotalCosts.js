import React from 'react'; 

function totalCosts (props) {
    let total= 0
    return (
        <div>{ props.products.map ((item, key) => {
            total += (props.products[key].price * props.products[key].quantity)
                return (
                    null
                )
                    
            })}
            <h2>Total: {total.toFixed(2)}</h2> 
            {total > 500 ? <div className="free-shipping"> Free Shipping! </div> 
             : <div className="no-free-shipping"> No free shipping, yet... (from $500)</div> }
        </div>
               )
}

export default totalCosts

