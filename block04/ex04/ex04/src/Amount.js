import React from 'react'; 

class Amount extends React.Component {
 
state = {
        quantity : 0
}

componentDidMount(){
    this.setState({
        quantity:this.props.quantity
    })
}

changeAmount (event, key) {
    //somehowe I need to send the 'key' of this article back to the updateAmount function, 
    //so that function knows off which item we change the amount
    event.preventDefault()
    let newQuantity = parseInt(event.target.value)
    // let indexItem = event.target.index

    this.setState ({
        quantity : newQuantity
     })  
    this.props.updateAmount(newQuantity, key)
    console.log(newQuantity)
}

 render () {
    return (
        <div>
            <p> Amount: {this.state.quantity} </p>
            <p style={changeAmount}> Change amount to: 
            <form>
                        {/* When I submit the form the number disapears again, how is that possible?  */}
                <input onChange={this.changeAmount.bind(this)} key={this.props.key} />
                <button>Add</button>
                 
                    </form>  
            </p>
          
        </div>
        )
 }
}
 
const changeAmount = {
    display: 'flex'
}

// 

export default Amount