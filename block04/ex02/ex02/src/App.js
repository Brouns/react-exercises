import React from 'react';
import './App.css'; 

class Converter extends React.Component {

state = {
      dollar: 0,
      euro: 0,
      exchangeRate: 1, 
    } 
  
  componentDidMount() {

    fetch(`http://www.apilayer.net/api/live?access_key=a984cc1c28db10d1002b2e52a664c0c5`)
      .then( res => res.json())
      .then( response =>  
            this.setState({
              exchangeRate : response.quotes.USDEUR
            })
      )    
      .catch( error => console.log(error))
      }

  EuroToDollar (event) {
    console.log(this.state)
        let newEuro = parseInt(event.target.value)
        console.log(this)
        let dollar = newEuro * this.state.exchangeRate;
        this.setState({
          dollar: dollar.toFixed(2)
        })
      }

  DollarToEuro (event) {
        let newDollar = parseInt(event.target.value) 
        let euro = newDollar / this.state.exchangeRate;
        this.setState ({
            euro: euro.toFixed(2)
          }); 
      
      }

  render () {

    console.log(this.state)
    return (
      <div className="App">
        <h1> Money converter </h1>
        <div className="converter">
              <div className="euroToDollar">
                  <h2>Amount of euro: 
                        <input className ="inputEuro" onChange={this.EuroToDollar.bind(this)}/>
                        <h3>{`is ${this.state.dollar} dollar`}</h3>
                  </h2>
              </div>
              <div className="dollarToEuro">
                  <h2>Amount of Dollar: 
                        <input className ="inputDollar" onChange={this.DollarToEuro.bind(this)}/>
                        <h3> {`is ${this.state.euro} euro`} </h3>
                  </h2>
              </div>
        </div>
      
      </div>
    )
  }
  

} 
  
export default Converter;



//WORKED!!
// ## Exercise 2

// Create a euro to dollar (and viceversa) convertor. It needs to take the input from one component and then render the converted value from a child component.

// It is up to you which component does the actual conversion...

// Extra:
// - Use an external API to get real exchanges rates, you can use this one: https://exchangeratesapi.io/

// > Use js fetch to get the data from the API: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

// ***Your solution goes to the ex02 folder***


      // fetch("https://exchangeratesapi.io/")
      //   .then( res => {
      //     return res.json()
      //   })
      //   .then(response => {
      //        console.log(response)
      //       // Initialized with 'EUR' because the base currency is 'EUR'
      //       // and it is not included in the response
      //       const currencyAr = ["EUR"]
      //       for (const key in response.data.rates) {
      //           currencyAr.push(key)
      //       }
      //       this.setState({ currencies: currencyAr.sort() })
      //   })
      //   .catch(err => {
      //       console.log("Opps", err.message);
      //   });




    // fetch("https://api.exchangeratesapi.io/latest?base=USD")
    // .then(response => {
    // const currencyAr = ["USD"];
    //   for (const key in response.rates) {
    //     currencyAr.push(key);
    //   }
    //   this.setState({ currencies: currencyAr });
    // });
  
// }

 // const currencyAr = ["EUR"]
            //       for (const key in response.quotes) {
            //            currencyAr.push(key)
            //  }
            //  this.setState({ currencies: currencyAr.sort() })
        