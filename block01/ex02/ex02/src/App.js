import React from 'react';
import Header from './Header'; 
import ProductsList from './ProductsList'; 
import Footer from './Footer'; 
import BestSeller from './BestSeller'; 
import SingleProduct from './SingleProduct'; 
import './App.css'; 

const Main = () =>  {

    const products = [
      {
        product    : 'flash t-shirt',
        price      :  27.50,
        category   : 't-shirts',
        bestSeller :  false,
        image      : 'https://www.juniba.pk/wp-content/uploads/2018/02/the-flash-distressed-logo-t-shirt-black.png',
        onSale     :  true
      },
      {
        product    : 'batman t-shirt',
        price      :  22.50,
        category   : 't-shirts',
        bestSeller :  true,
        image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
        onSale     :  false
      },
      {
        product    : 'superman hat',
        price      :  13.90,
        category   : 'hats',
        bestSeller :  true,
        image      : 'https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg',
        onSale     :  false
      }
  ]


    return (
        <div> 
            <Header />
            <div className= "Best">
            <ProductsList products = {products} />
            </div>
            <Footer />
            
        </div> 
    )
    
  }


export default Main;

// Worked!!!

// ## Exercise 5

// Refactor the eCommerce clone you did in the previous block to use props.

// You should now use the Main/List/Item pattern which will be App/ProductsList/SingleProduct components for the lists of products. 

// Declare the list of products in App and pass it to ProductsList.

// Loop in ProductsList and for each iteration render the product in SingleProduct. 

// > Optional with props or `useContext`

