import React from 'react'; 

function ProductListItem (props) {
    console.log(props.product.onSale)

    return <div>
        <h3>{props.product}</h3>
        <img 
        height = {100}
        title = {props.product}
        src = {`${props.product.image}`}
        />
        <div> ${props.price}</div>
        <h4>{props.product.onSale ? <span>SALE!!!</span> : null} </h4>

        <div> 
            <button> Buy </button>
        </div>
    </div>
}

export default ProductListItem