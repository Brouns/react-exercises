import React from 'react';

function BestSeller (props) {
console.log(props)
        return <div className = "BestItem">
            <h3>{props.item.product}</h3>
            <img height = {100}
                title = {props.item.product}
                src = {`${props.item.image}`}
                />
                <div> ${props.item.price}</div>
                <h4>{props.item.onSale ? <span>SALE!!!</span> : null} </h4>

                <div> 
                    <button> Buy </button>
                </div>
            </div>
            
    }

export default BestSeller; 
