
    
//     Create a brand new React application with 
//     'React create App'. This app needs to display a 
//     list of product categories from an array. 
//     The categories are 't-shirts', 'hats', 
//     'shorts', 'shirts', 'pants', 'shoes'. 
//     In order to display them you will need 
//     to use Array.map

// ***Your solution goes to the ex01 folder***


import React from 'react'

const list = () => { 

  const items = ['t-shirts', 'hats','shorts', 'shirts', 'pants', 'shoes']

  const itemsInList = items.map ((item, i) => {
    return <p key = {i}>{item}</p>
  })

return <div>{itemsInList}</div>

}

export default list