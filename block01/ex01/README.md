import React from 'react';


const App = () => {
//Create a React functional component called App

    var myName ='Antonello' 
    //make sure you defined your variable outside the return

    return (
    //ALL that you display needs to be inside your return
      <div className="App">
      {/*render a DIV with a h2 inside*/}

        <h2>Welcome to React from {myName}</h2>
      {/* In curly braces we execute ANY valid JS */}
      
      </div>
    )
  }

export default App; //Export your component`}

Exercise 1
Create a brand new React application with 'React create App'. This app needs to display a list of product categories from an array. The categories are 't-shirts', 'hats', 'shorts', 'shirts', 'pants', 'shoes'. In order to display them you will need to use Array.map



const element = <h1>Hello, world!</h1>

