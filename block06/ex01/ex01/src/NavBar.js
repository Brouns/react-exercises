import React from 'react'

export default ({changePage}) => {

        return (
            <ul style={navStyle}
                onClick = {(e) => changePage(e.target.textContent)} >
                <li>Home</li>
                <li>About</li>
                <li>Contact</li>
                <li>Gallary</li>
            </ul>
        )
    }

const navStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    height: '30px',
    color: 'white',
    backgroundColor: '#90EE90',
    listStyle:'none',
    padding:'2vw 10vw',
    margin:'0'

}
