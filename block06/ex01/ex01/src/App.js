import React from 'react';
import NavBar from './NavBar'; 
import Home from './Home';
import Gallary from './Gallary';
import Contact from './Contact';
import About from './About'; 

class App extends React.Component {

  state = {
    currentPage : 'Home'
  }

changePage = currentPage => {
 this.setState({currentPage : currentPage})
}

render () {
  let current; 

  if (this.state.currentPage === 'Home') { current = <Home /> }
  if (this.state.currentPage === 'Gallary') { current = <Gallary /> }
  if (this.state.currentPage === 'Contact') {current = <Contact /> }
  if (this.state.currentPage === 'About') {current = <About /> }

  return (
    <div>
    <NavBar changePage={this.changePage}/> 
      {current}
    </div>
  )
    
  
}
}

export default App;


//WORKED!!