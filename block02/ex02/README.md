
## Exercise 2

Create a parent component inside which you define two arrays of equal length, the first will have 5 firstnames and the second 5 lastnames.

Create 2 children components – one to display the firstnames and one to display the lastnames so that they are rendered side by side with matching order.


create-react-app "block02ex02"
cd "block02ex02"
npm start
