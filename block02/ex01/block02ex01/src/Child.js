
import React from 'react'


class Child extends React.Component{
    
  render(){
    console.log(this.props)
    return (
      <div>
            {
              <h1>"Hello {this.props.name}, I am  the child component!"</h1>
            }
          </div>
    )
  }
}
export default Child
