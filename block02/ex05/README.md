## Exercise 5

Refactor the eCommerce clone you did in the previous block to use props.

You should now use the Main/List/Item pattern which will be App/ProductsList/SingleProduct components for the lists of products. 

Declare the list of products in App and pass it to ProductsList.

Loop in ProductsList and for each iteration render the product in SingleProduct. 

> Optional with props or `useContext`

// I already did this the first time.